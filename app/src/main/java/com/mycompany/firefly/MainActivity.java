package com.mycompany.firefly;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //start the display location activity
    public void displayLocation(View view) {
        Intent intent = new Intent(this, displayLocation.class);
        startActivity(intent);
    }
}
