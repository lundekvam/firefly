package com.mycompany.firefly;

/*
 * Created by Lundekvam on 13.09.2015.
 *
 * Code from theNewBoston - Bucky Roberts (January 6, 2015):
 * Youtube (Android App Development for Beginners: video 49-54): https://www.youtube.com/watch?v=_vQaOvPsLko&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=49
 * Website: https://www.thenewboston.com/forum/topic.php?id=3767
 */
public class Search {
    private int _id;
    private String _locationName;

    public Search(){
    }

    public Search(String locationName){
        this._locationName = locationName;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void set_locationName(String _locationName) {
        this._locationName = _locationName;
    }

    public int get_id() {
        return _id;
    }

    public String get_locationName() {
        return _locationName;
    }

}
