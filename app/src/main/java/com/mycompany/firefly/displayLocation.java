package com.mycompany.firefly;

import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

/*
 * CODE FROM:
 * SQLite Database - theNewBoston
 * theNewBoston - Bucky Roberts (January 6, 2015):
 * Youtube (Android App Development for Beginners: video 49-54): https://www.youtube.com/watch?v=_vQaOvPsLko&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=49
 * Website: https://www.thenewboston.com/forum/topic.php?id=3767
 *
 * Search Location - Tech Academy
 * Tech Academy - (June 6, 2015):
 * Youtube (Google Maps Tutorial:Part 2 Current location & Search Address): https://www.youtube.com/watch?v=dr0zEmuDuIk
 *
 * Current Location - Salman
 * Stackoverflow - Salman (April 25, 2015):
 * Website: http://stackoverflow.com/questions/29868121/how-do-i-zoom-in-automatically-to-the-current-location-in-google-map-api-for-and
 */

public class displayLocation extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    /*
    * database text
    * FROM: SQLite Database - theNewBoston
    */
    EditText searchInput;
    TextView searchText;
    MyDBHandler dbHandler;

    /*
     * maps
     * FROM: Current Location - Salman
     */
    public static final String TAG = displayLocation.class.getSimpleName();
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    //info text
    TextView textLat;
    TextView textLong;
    TextView textAlt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_location);
        setUpMapIfNeeded();

        //FROM: SQLite Database - theNewBoston
        searchInput = (EditText) findViewById(R.id.searchInput);
        searchText = (TextView) findViewById(R.id.searchText);
        dbHandler = new MyDBHandler(this, null, null, 1);
        printDatabase();

        //FROM: Current Location - Salman
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        //get location info
        textLat = (TextView)findViewById(R.id.textLat);
        textLong = (TextView)findViewById(R.id.textLong);
        textAlt = (TextView)findViewById(R.id.textAlt);

        //tabs
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();
        //map tab
        TabHost.TabSpec tabSpec = tabHost.newTabSpec("map");
        tabSpec.setContent(R.id.tab1);
        tabSpec.setIndicator("Map");
        tabHost.addTab(tabSpec);
        //info tab
        tabSpec = tabHost.newTabSpec("info");
        tabSpec.setContent(R.id.tab2);
        tabSpec.setIndicator("Info");
        tabHost.addTab(tabSpec);
        //database tab
        tabSpec = tabHost.newTabSpec("db");
        tabSpec.setContent(R.id.tab3);
        tabSpec.setIndicator("Db");
        tabHost.addTab(tabSpec);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    public void searchButtonClicked(View view){
        /*
         * searching for a new location
         * FROM: Search Location - Tech Academy
         */
        EditText location_tf = (EditText)findViewById(R.id.searchInput);
        String location = location_tf.getText().toString();
        List<Address> addressList = null;
        if (location != null || !location.equals("")){
            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(location, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Address address = addressList.get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            mMap.addMarker(new MarkerOptions().position(latLng).title("You searched for this"));
            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        }

        /*
         * storing the new location in the database
         * FROM: SQLite Database - theNewBoston
         */
        Search locations = new Search(searchInput.getText().toString());
        dbHandler.addLocation(locations);
        printDatabase();
    }

    //Delete items
    public void deleteButtonClicked(View view){
        /*
         * delete the row in the database that matches the input text in the search field when button is clicked
         * FROM: SQLite Database - theNewBoston
         */
        String inputText = searchInput.getText().toString();
        dbHandler.deleteLocation(inputText);
        printDatabase();
    }

    /*
     * Print the database
     * FROM: SQLite Database - theNewBoston
     */
    public void printDatabase(){
        String dbString = dbHandler.databaseToString();
        searchText.setText(dbString);
        searchInput.setText("");
    }

    /*
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /*
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.setMyLocationEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
    }

    //FROM: Current Location - Salman
    private void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());

        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        double currentAltitude = location.getAltitude();

        //current location info as string
        textLat.setText("Latitude: " + Double.toString(currentLatitude));
        textLong.setText("Longitude: " + Double.toString(currentLongitude));
        textAlt.setText("Altitude: " + Double.toString(currentAltitude));

        LatLng latLng = new LatLng(currentLatitude, currentLongitude);

        //add marker to my current location
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title("You are here!");
        mMap.addMarker(options);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
    }

    //FROM: Current Location - Salman
    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        else {
            handleNewLocation(location);
        }
    }

    //FROM: Current Location - Salman
    @Override
    public void onConnectionSuspended(int i) {

    }

    //FROM: Current Location - Salman
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    /*
     * Google Play services can resolve some errors it detects.
     * If the error has a resolution, try sending an Intent to
     * start a Google Play services activity that can resolve
     * error.
     */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            /*
             * Thrown if Google Play services canceled the original
             * PendingIntent
             */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
        /*
         * If no resolution is available, display a dialog to the
         * user with the error.
         */
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    //FROM: Current Location - Salman
    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }
}
