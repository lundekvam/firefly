package com.mycompany.firefly;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;

/*
 * Created by Lundekvam on 13.09.2015.
 *
 * Code from theNewBoston - Bucky Roberts (January 6, 2015):
 * Youtube (Android App Development for Beginners: video 49-54): https://www.youtube.com/watch?v=_vQaOvPsLko&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=49
 * Website: https://www.thenewboston.com/forum/topic.php?id=3767
 */
public class MyDBHandler extends SQLiteOpenHelper {

    //database info
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "searchDB.db";
    public static final String TABLE_SEARCHES = "searches";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_LOCATIONNAME = "locationName";

    //we need to pass database information along to superclass
    public MyDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    //add table to database with id and locationName columns
    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_SEARCHES + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_LOCATIONNAME + " TEXT " +
                ");";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEARCHES);
        onCreate(db);
    }

    //add a new row to the database
    public void addLocation(Search locations){
        ContentValues values = new ContentValues();
        values.put(COLUMN_LOCATIONNAME, locations.get_locationName());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_SEARCHES, null, values);
        db.close();
    }

    //delete a location from the database
    public void deleteLocation(String locationName){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_SEARCHES + " WHERE " + COLUMN_LOCATIONNAME + "=\"" + locationName + "\";");
    }

    //get everything in the database
    public String databaseToString(){
        String dbString = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_SEARCHES + " WHERE 1";

        //cursor points to a location in the results
        Cursor c = db.rawQuery(query, null);
        //move to the first row in the results
        c.moveToFirst();

        //position after the last row means the end of the results
        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex("locationName")) != null) {
                dbString += c.getString(c.getColumnIndex("locationName"));
                dbString += "\n";
            }
            c.moveToNext();
        }
        db.close();
        return dbString;
    }

}